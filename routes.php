<?php
Route::group(array('prefix' => 'api', 'namespace' => 'App\Modules\Users\Controllers'), function () {
    Route::group(array('before' => 'csrf_json'), function () {
        Route::group(array('before' => 'auth'), function () {;
            Route::get('/thong-users/{userId}', 'UsersController@get');
            Route::post('/thong-users/profile', 'UsersController@saveProfile');
            Route::post('/thong-users/settings', 'UsersController@saveSettings');
            Route::get('/thong-users', 'UsersController@search');
            Route::post('/thong-users', 'UsersController@save');
            Route::delete('/thong-users/{userId}', 'UsersController@delete');
        });
    });
});

Route::group(array('prefix' => 'api', 'namespace' => 'App\Modules\Thong\Controllers'), function () {
    Route::get('/abc', 'ThongUsersController@search');
});