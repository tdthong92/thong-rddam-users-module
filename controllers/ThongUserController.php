<?php namespace App\Modules\Thong\Controllers;

use Auth, Controller, Input, Validator, DB, Config, Response, Hash, Exception, File, URL, Session, App, Schema;
use App\Modules\Users\Models\UserRole;
use App\Modules\Users\Models\UserMeta;
use App\Modules\Users\Models\Users;
use App\Modules\Crm\Models\Activities;
use App\Modules\Crm\Models\ActivityParticipants;
use App\Modules\Crm\Models\Notes;
use App\Modules\Crm\Models\TimelineEvents;
use App\Modules\Crm\Models\ActivityMeta;
use App\Library\AuthHelper;
use App\Library\Helper;
use mainConfig;

class ThongUserController extends Controller {
    private $mainConfigObj;

    public function search() {
        $params = Helper::camelCaseToUnderscore(Input::all(), 1, 1);
        if(!empty($params['u_type']) && $params['u_type'] != 'All') {
            $uType = Config::get('common-data.USER_TYPE.'.ucfirst($params['uType']));
        } else{
            $uType = 'All';
        }
        if($uType == 'All') {
            $query = Users::where('user_type', '!=', '');
        } else {
            $query = Users::where('user_type', '=', $uType);
        }
        if (!empty($params['s'])) {
            $query->where('name', 'LIKE', '%' . $params['s'] . '%');
        }
        if (!empty($params['sort_fields'])) {
            $params['sort_fields'] = Helper::camelCaseToUnderscore($params['sort_fields']);
            $sortMeta = false;
            if (Schema::hasColumn('users', $params['sort_fields'])) {
                $query->orderBy($params['sort_fields'],
                    (isset($params['sort_directions']) && strtolower($params['sort_directions']) == 'desc' ? 'DESC' : 'ASC'));
            } else {
                $sortMeta = true;
            }
        }
        $isAll = !empty($params['all']) ? 1 : 0;
        if (!$isAll) {
            $page = isset($params['page_number']) && $params['page_number'] > 0 ? $params['page_number'] : Config::get('settings.DEFAULT_PAGE');
            $pageSize = isset($params['pageSize']) && $params['pageSize'] > 0 ? $params['pageSize'] : Config::get('settings.PER_PAGE');
            $ret['total'] = $query->count();
            $arrItems = $query->skip($pageSize * ($page - 1))->take($pageSize)->get();
        } else {
            $arrItems = $query->get();
        }
        $ret['items'] = array();
        $fields = !empty($params['fields']) && is_array($params['fields']) && $params['fields'] ? $params['fields'] : 'ALL';
        foreach ($arrItems as $item) {
            if ($fields == 'ALL') {
                $ret['items'][] = $item->getDetail();
            } else {
                $val = array();
                foreach ($fields as $field) {
                    if (!is_null($item->$field)) {
                        $val[$field] = $item->$field;
                    }
                }
                if ($val) {
                    $ret['items'][] = $val;
                }
            }
        }
        return Response::json(Helper::underscoreToCamelCase($ret, 1, 1));
    }
}