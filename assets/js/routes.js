'use strict';
angular.module('app').config(
    ['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.thong-user', {
                abstract: true,
                url: 'thong-users',
                template: '<ui-view/>',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                'inc/common/utils/js/filters/string.js',
                                'packages/module/rddam-users-module/assets/js/services/users.js',
                                'packages/module/rddam-users-module/assets/capability/js/services/roles.js', //New added by vijay
                                'packages/module/rddam-users-module/assets/auth/js/directives/ng-capability.js', //New added by vijay
                            ]);
                        }
                    ]
                }
            })
            .state('app.thong-user.list', {
                url: '',
                templateUrl: 'packages/module/rddam-users-module/assets/tpl/list.html',
                resolve: {
                    deps: ['$ocLazyLoad', 'uiLoad',
                        function($ocLazyLoad, uiLoad) {
                            return uiLoad.load([
                                'inc/common/utils/js/filters/date.js',
                                'packages/module/rddam-users-module/assets/js/controllers/list.js'
                            ]).then(function() {
                                return $ocLazyLoad.load('ngGrid');
                            })
                        }
                    ]
                }
            })
            .state('app.thong-user.add', {
                url: '/add',
                templateUrl: 'packages/module/rddam-users-module/assets/tpl/form.html',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                'inc/common/utils/js/filters/date.js',
                                'packages/module/rddam-users-module/assets/js/controllers/form.js'
                            ]);
                        }
                    ]
                }
            })

    }
    ]);