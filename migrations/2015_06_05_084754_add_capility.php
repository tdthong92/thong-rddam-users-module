<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCapility extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        $env = App::environment();
        if($env == 'ifinancialadvise') {
            $insertData [] = array('capability_key' => 'thong_show_all_user', 'capability_name' => 'Thong Show All User');
            DB::table('capabilities')->insert($insertData);
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
